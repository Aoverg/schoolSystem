#include <iostream>
#include <fstream>

class person
{
protected:
	std::string firstName;
	std::string lastName;
	int age;
	std::string adress;
	std::string phoneNumber;

public:
	person(std::string firstName, std::string lastName, int age)
	{
		this->firstName = firstName;
		this->lastName = lastName;
		this->age = age;
	}
};

class course
{
protected:
	int coursePoints;
	std::string subject;

public:
	course(int coursePoints, std::string subject)
	{
		this->coursePoints = coursePoints;
		this->subject = subject;
	}
};

class Teacher : person
{
protected:
	int salary;
	std::string department;

public:
	Teacher(std::string firstName, std::string lastName, int age, int salary, std::string department)
		: person(firstName, lastName, age)
	{
		this->department = department;
		this->salary = salary;
	}
};

class student : person
{
protected:
	int grade;
	int schoolYear;

public:
	student(std::string firstName, std::string lastName, int age, int grade, int schoolYear)
		: person(firstName, lastName, age)
	{
		this->grade = grade;
		this->schoolYear = schoolYear;
	}
};


int main()
{
	
}